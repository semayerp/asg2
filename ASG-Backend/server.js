const express = require("express");
//const promMid = require('express-prometheus-middleware');
const bodyParser = require("body-parser");
const cors = require("cors");
const dbConfig = require("./app/config/db.config");

const app = express();

var corsOptions = {
  origin: "https://greenfrontend.localhost",   // use -> ng serve --port 8100 to run the frontend/angular
  
};

app.use(cors());
//prometheus middlware mounting
// app.use(promMid({
//   metricsPath: '/metrics',
//   collectDefaultMetrics: true,
//   requestDurationBuckets: [0.1, 0.5, 1, 1.5],
//   requestLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
//   responseLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],

//  }))


// prometheus ends here

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
const Role = db.role;
console.log('#########')
console.log(process.env.MONGO_URI)
console.log('#########')
db.mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to ASG backend application." });
});

// routes
require("./app/routes/auth.routes")(app);
require("./app/routes/user.routes")(app);
require("./app/routes/activity-log.routes")(app);
require("./app/routes/crop.routes")(app);
require("./app/routes/farm-location.routes")(app);
require("./app/routes/fertilizer.routes")(app);
require("./app/routes/plant.routes")(app);
require("./app/routes/subscriber.routes")(app);
require("./app/routes/harvest.routes")(app);


// set port, listen for requests
const PORT = process.env.PORT || "8000";
const HOST = process.env.HOST || '0.0.0.0';
app.listen(PORT,HOST, () => {
  console.log(`Server is running on ${HOST}:${PORT}.`);
});

function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user' to roles collection");
      });

      new Role({
        name: "extension"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'extension' to roles collection");
      });

      new Role({
        name: "admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });

      new Role({
        
        name: "seeder"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'seeder' to roles collection");
      });
    }
  });
}