const db = require("../models");
const Fertilizer = db.fertilizers;

// Create and Save a new Fertilizer
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create a Fertilizer
  const fertilizer = new Fertilizer({
    name: req.body.name,
    nutrient:  req.body.nutrient,
    type :  req.body.type 
    
    
    
     });

  // Save Fertilizer in the database
  fertilizer
    .save(fertilizer)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Fertilizer."
      });
    });
};

// Retrieve all Fertilizers from the database.
exports.findAll = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {};

  Fertilizer.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving fertilizers."
      });
    });
};

// Find a single Fertilizer with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Fertilizer.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found Fertilizer with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Fertilizer with id=" + id });
    });
};

// Update a Fertilizer by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Fertilizer.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Fertilizer with id=${id}. Maybe Fertilizer was not found!`
        });
      } else res.send({ message: "Fertilizer was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Fertilizer with id=" + id
      });
    });
};

// Delete a Fertilizer with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Fertilizer.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Fertilizer with id=${id}. Maybe Fertilizer was not found!`
        });
      } else {
        res.send({
          message: "Fertilizer was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Fertilizer with id=" + id
      });
    });
};

// Delete all Fertilizers from the database.
exports.deleteAll = (req, res) => {
  Fertilizer.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Fertilizers were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all fertilizers."
      });
    });
};

// Find all published Fertilizers
exports.findAllPublished = (req, res) => {
  Fertilizer.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving fertilizers."
      });
    });
};
