const db = require("../models");
const Plant = db.plants;

// Create and Save a new Plant
exports.create = (req, res) => {
  // Validate request
  if (!req.body.operation) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create a Plant
  const plant = new Plant({
    
        farmer: req.body.farmer,
        crop:req.body.crop,
        operation: req.body.operation,
        quantity:req.body.quantity,
        quality:req.body.quality ,
        type:req.body.type,
        variety:req.body.variety,
        rainfall:req.body.rainfall,
        yearOfrelease:req.body.yearOfrelease,
        seedRate:req.body.seedRate,
        additionalInfo:req.body. additionalInfo,
        owner: req.userId, 
     });

  // Save Plant in the database
  plant
    .save(plant)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Plant."
      });
    });
};

// Retrieve all Plants from the database.
exports.findAll = (req, res) => {
  const operation = req.query.operation;
  var condition = operation ? { operation: { $regex: new RegExp(operation), $options: "i" } } : {};

  Plant.find({owner: req.userId})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving plants."
      });
    });
};

// Find a single Plant with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Plant.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found Plant with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Plant with id=" + id });
    });
};

// Update a Plant by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Plant.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Plant with id=${id}. Maybe Plant was not found!`
        });
      } else res.send({ message: "Plant was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Plant with id=" + id
      });
    });
};

// Delete a Plant with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Plant.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Plant with id=${id}. Maybe Plant was not found!`
        });
      } else {
        res.send({
          message: "Plant was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Plant with id=" + id
      });
    });
};

// Delete all Plants from the database.
exports.deleteAll = (req, res) => {
  Plant.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Plants were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all plants."
      });
    });
};

// Find all published Plants
exports.findAllPublished = (req, res) => {
  Plant.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving plants."
      });
    });
};

exports.PlantSpecificCount =  (req,res,next) =>{
  Plant.find({ owner: req.userId })
  .exec((err, plant) => {
   let count = plant.length;
   res.status(200).send((count).toString());
 })
};

exports.PlantSpecificCountSum =  (req,res,next) =>{
  let sum = Plant.aggregate(
    [ { $match : { _id : req.userId  } } ,

      { $group: { _id: "$quantitiy", count: { $sum: 1 } } }

    ]
    
);
console.log(sum);
  }

  exports.PlantationTotalEach = (req,res,next) =>{
    
    Plant.aggregate([
  
     
      { $group: { _id:"$owner",total:{ $sum:"$quantity"
          }
  
        }
      },
      {"$sort": {"_id": -1}},
     {"$limit": 5}
     
    ],
    (error,data)=>{
        
        if (error){
         
          res.json(error);
        }
        else 
        res.json(data);
      }
    
    )
  
  }

  exports.allPlantTotal = (rep,res,next) =>{
    
    Plant.aggregate([
      {
        $group: { _id:1,
          
        
        total:{
            $sum:"$quantity"
          }
  
        }
      }
     
    ],
    (error,data)=>{
      if (error){
        return next(error);
      } else {
        res.json(data);
      }
  
    }
    )
  
  }

  exports.plantCount = (rep,res,next) =>{
    
    Plant.aggregate([
      {
        $group: {
          _id:1,
          count:{
            $sum:1
          }
        }
      }
     
    ],
    (error,data)=>{
      if (error){
        return next(error);
      } else {
        res.json(data);
      }
  
    }
    )
  
  }
